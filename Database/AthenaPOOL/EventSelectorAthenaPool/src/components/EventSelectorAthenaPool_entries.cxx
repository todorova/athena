#include "../AthenaPoolAddressProviderSvc.h"
#include "../DoubleEventSelectorAthenaPool.h"
#include "../EventSelectorAthenaPool.h"
#include "../CondProxyProvider.h"

DECLARE_COMPONENT( AthenaPoolAddressProviderSvc )
DECLARE_COMPONENT( EventSelectorAthenaPool )
DECLARE_COMPONENT( DoubleEventSelectorAthenaPool )
DECLARE_COMPONENT( CondProxyProvider )

